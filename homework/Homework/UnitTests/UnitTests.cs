﻿using Homework;
using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace UnitTests
{
    public class UnitTest
    {
        private Mock<IAction> action;

        private Mock<IAccountRepository> _accountRepository;

        private AccountActivityService service;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            this.action = new Mock<IAction>();
            this.action.Setup(provider => provider.Execute()).Returns(true);
        }

        [SetUp]
        public void SetUp()
        {
            this._accountRepository = new Mock<IAccountRepository>();
            service = new AccountActivityService(this._accountRepository.Object);
        }

        [Test]
        [Category("Account")]
        public void Account_Creation()
        {
            int ID = 55;

            Account account = new Account(ID);

            Assert.That(account, Is.Not.Null);

            Assert.That(account.IsConfirmed, Is.False);

            Assert.That(account.IsRegistered, Is.False);

            Assert.AreEqual(account.Id, ID);
        }

        [Test]
        [Category("Account")]
        public void Account_Register()
        {
            Account account = new Account(0);

            Assert.That(account.IsRegistered, Is.False);

            account.Register();

            Assert.That(account.IsRegistered, Is.True);

            Assert.That(account.IsConfirmed, Is.False);
        }

        [Test]
        [Category("Account")]
        public void Account_Activate()
        {
            Account account = new Account(0);

            Assert.That(account.IsConfirmed, Is.False);

            account.Activate();

            Assert.That(account.IsConfirmed, Is.True);

            Assert.That(account.IsRegistered, Is.False);
        }

        [Test]
        [Category("AccountTakeAction")]
        public void Account_TakeAction_Successfull()
        {
            Account account = new Account(0);

            account.Register();
            account.Activate();

            bool success = account.TakeAction(this.action.Object);

            this.action.Verify(provide => provide.Execute(), Times.Once);
            this.action.VerifyNoOtherCalls();

            Assert.That(success, Is.True);
            Assert.AreEqual(account.ActionsSuccessfullyPerformed, 1);
        }

        [Test]
        [Category("AccountTakeAction")]
        public void Account_TakeAction_Unsuccessfull()
        {
            Account account = new Account(0);

            Assert.That(() => account.TakeAction(this.action.Object), Throws.TypeOf<InactiveUserException>());
        }

        [TestCase(0, ActivityLevel.None)]
        [TestCase(1, ActivityLevel.Low)]
        [TestCase(21, ActivityLevel.Medium)]
        [TestCase(41, ActivityLevel.High)]
        [Category("ActivityServiceGetActivity")]
        public void ActivityService_GetActivity_Succesffull(int activityNumber, ActivityLevel expectedLevel)
        {
            Account account = new Account(1);
            account.Activate();

            for(int i = 0; i < activityNumber; i++)
            {
                account.TakeAction(this.action.Object);
            }

            this._accountRepository.Setup(provider => provider.Get(It.IsAny<int>())).Returns(account);

            ActivityLevel level = service.GetActivity(1);

            Assert.AreEqual(expectedLevel, level);

            this._accountRepository.Verify(provider => provider.Get(It.IsAny<int>()), Times.Once);
            this._accountRepository.VerifyNoOtherCalls();
        }

        [Test]
        [Category("ActivityServiceGetActivity")]
        public void ActivityService_GetActivity_Unsuccesffull()
        {
            this._accountRepository.Setup(provider => provider.Get(It.IsAny<int>())).Returns((Account)null);

            Assert.That(() => service.GetActivity(1), Throws.TypeOf<AccountNotExistsException>());
        }

        [TestCase(3, 5, 6, 7, ActivityLevel.None, 3)]
        [TestCase(3, 5, 6, 7, ActivityLevel.Low, 5)]
        [TestCase(3, 5, 6, 7, ActivityLevel.Medium, 6)]
        [TestCase(3, 5, 6, 7, ActivityLevel.High, 7)]
        [TestCase(0, 0, 0, 0, ActivityLevel.None, 0)]
        [TestCase(0, 0, 0, 0, ActivityLevel.Low, 0)]
        [TestCase(0, 0, 0, 0, ActivityLevel.Medium, 0)]
        [TestCase(0, 0, 0, 0, ActivityLevel.High, 0)]
        [Category("ActivityServiceActivityAmount")]
        public void ActivityService_ActivityAmount_Successfull(int numOfNone, int numOfLow, int numOfMedium, int numOfHigh, ActivityLevel levelToBeChecked, int expectedValue)
        {
            List<Account> accounts = CreateAccountList(numOfNone, numOfLow, numOfMedium, numOfHigh);
            this._accountRepository.Setup(provider => provider.GetAll()).Returns(accounts);
            this._accountRepository.Setup(provider => provider.Get(It.IsAny<int>())).Returns<int>(x => accounts[x]);

            int result = this.service.GetAmountForActivity(levelToBeChecked);

            Assert.AreEqual(expectedValue, result);

            this._accountRepository.Verify(provider => provider.Get(It.IsAny<int>()), Times.Exactly(accounts.Count));
            this._accountRepository.Verify(provider => provider.GetAll(), Times.Once);
            this._accountRepository.VerifyNoOtherCalls();
        }

        [Test]
        [Category("ActivityServiceActivityAmount")]
        public void ActivityService_ActivityAmount_Unsuccessfull()
        {
            this._accountRepository.Setup(provider => provider.GetAll()).Returns(new List<Account>());
            this._accountRepository.Setup(provider => provider.Get(It.IsAny<int>())).Returns((Account)null);

            int result = this.service.GetAmountForActivity(ActivityLevel.None);

            Assert.AreEqual(0, result);
            this._accountRepository.Verify(provider => provider.GetAll(), Times.Once);
            this._accountRepository.Verify(provider => provider.Get(It.IsAny<int>()), Times.Never);
            this._accountRepository.VerifyNoOtherCalls();
        }

        private List<Account> CreateAccountList(int numOfNone, int numOfLow, int numOfMedium, int numOfHigh)
        {
            List<Account> accounts = new List<Account>();
            for(int i = 0; i < numOfNone; i++)
            {
                accounts.Add(new Account(i));
            }

            Account accToBeAdded;

            for (int i = 0; i < numOfLow; i++)
            {
                accToBeAdded = new Account(numOfNone + i);
                accToBeAdded.Activate();
                accToBeAdded.TakeAction(this.action.Object);
                accounts.Add(accToBeAdded);
            }

            for(int i = 0; i < numOfMedium; i++)
            {
                accToBeAdded = new Account(numOfNone + numOfLow + i);
                accToBeAdded.Activate();
                for (int l = 0; l < 21; l++)
                {
                    accToBeAdded.TakeAction(this.action.Object);
                }
                accounts.Add(accToBeAdded);
            }

            for (int i = 0; i < numOfHigh; i++)
            {
                accToBeAdded = new Account(numOfNone + numOfLow + numOfMedium + i);
                accToBeAdded.Activate();
                for (int l = 0; l < 41; l++)
                {
                    accToBeAdded.TakeAction(this.action.Object);
                }
                accounts.Add(accToBeAdded);
            }

            return accounts;
        }
    }
}
